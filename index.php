<?php
require 'vendor/autoload.php';

$message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu est convallis, 
			cursus ex nec, aliquam nulla. In congue hendrerit eros, sit amet ultricies enim 
			posuere et. Vestibulum id odio a nulla elementum auctor. Aenean varius sagittis 
			massa, vel accumsan arcu pellentesque sit amet. Nullam a risus ac nibh interdum 
			tincidunt eget ut massa. Ut ut semper enim. Curabitur ac justo ipsum. Vestibulum quis nisl orci..";

$listeArticles = array(
	array(	'titre' => 'alpha',
			'texte' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
	),
	array(	'titre' => 'beta',
			'texte' => 'In congue hendrerit eros, sit amet ultricies enim posuere et.',
	),
	array(	'titre' => 'omega',
			'texte' => 'Aenean varius sagittis massa, vel accumsan arcu pellentesque sit amet.',
	),		
);
			

try{
	$odf = new Odf("./sample.odt",array('PATH_TO_TMP' => $_SERVER["TMP"]));

	$odf->setVars('titre', 'Título', false, 'UTF8');

	$odf->setVars('message', $message, false, 'UTF8');

	$article = $odf->setSegment('articles');
	foreach($listeArticles AS $element) {
		$article->titreArticle($element['titre'], false, 'UTF8');
		$article->texteArticle($element['texte'], false, 'UTF8');
		$article->merge();
	}
	$odf->mergeSegment($article);

	// We export the file
	$odf->exportAsAttachedFile();
}
catch(Exception $e){
	echo '<pre>';
	print_r($e);
	echo '</pre>';
}
 
?>